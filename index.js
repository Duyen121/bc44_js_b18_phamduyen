var nNumberArr = [];
var realNumberArr = [];

function addNumToArr() {
  var nNumEl = document.querySelector("#number-n");
  var nNum = nNumEl.value * 1;
  nNumberArr.push(nNum);
  nNumEl.value = "";
  document.querySelector("#result-array").innerText = `${nNumberArr}`;
}

function calSumOfArr() {
  var arrSum = 0;
  for (let i = 0; i < nNumberArr.length; i++) {
    if (nNumberArr[i] > 0) {
      arrSum += nNumberArr[i];
    }
  }
  document.getElementById("array-sum").innerText = `Tổng số dương: ${arrSum}`;
}

function countPositiveNumOfArr() {
  var positiveNumArr = [];
  for (let i = 0; i < nNumberArr.length; i++) {
    if (nNumberArr[i] > 0) {
      positiveNumArr.push(nNumberArr[i]);
    }
  }
  document.getElementById(
    "positive-num-in-array"
  ).innerText = `Số dương: ${positiveNumArr.length}`;
}

function getMinOfArr() {
  var min = nNumberArr[0];
  for (let i = 0; i < nNumberArr.length; i++) {
    if (nNumberArr[i] < min) {
      min = nNumberArr[i];
    }
  }
  document.getElementById("array-min").innerText = `Số nhỏ nhất: ${min}`;
}

function getPositiveMinOfArr() {
  var min = nNumberArr[0];
  for (let i = 0; i < nNumberArr.length; i++) {
    if (nNumberArr[i] > 0 && nNumberArr[i] < min) {
      min = nNumberArr[i];
    }
  }
  document.getElementById(
    "array-positive-min"
  ).innerText = `Số dương nhỏ nhất: ${min}`;
}

function getEvenNumArr() {
  var evenNumArr = [];
  for (let i = 0; i < nNumberArr.length; i++) {
    if (nNumberArr[i] % 2 == 0) {
      evenNumArr.push(nNumberArr[i]);
    }
  }
  return evenNumArr;
}

function getLastEvenNumOfArr() {
  var lastEvenNum = "";
  var resultEl = document.getElementById("final-result");
  var evenNumArr = getEvenNumArr();

  if (evenNumArr.length > 0) {
    lastEvenNum = evenNumArr[evenNumArr.length - 1];
    resultEl.innerText = `Số chẵn cuối cùng: ${lastEvenNum}`;
  } else {
    resultEl.innerText = `Mảng không có số chẵn, trả về -1`;
  }
}

function swapItemsInArr() {
  var place1 = document.querySelector("#place-1").value * 1;
  var place2 = document.querySelector("#place-2").value * 1;
  var temp = nNumberArr[place1 - 1];
  nNumberArr[place1 - 1] = nNumberArr[place2 - 1];
  nNumberArr[place2 - 1] = temp;

  document.querySelector(
    "#new-array"
  ).innerText = `Mảng sau khi đổi chỗ: ${nNumberArr}`;
}

function bottomUpForArr() {
  var newArr = nNumberArr.sort(function (a, b) {
    return a - b;
  });
  document.getElementById(
    "bottom-up-array"
  ).innerText = `Mảng theo thứ tự tăng dần: ${newArr}`;
}

function isPrime(num) {
  for (let start = 2; num > start; start++) {
    if (num % start == 0) {
      return false;
    }
  }
  return num > 2;
}

function findfirstPrimeNumInArr() {
  var primeArr = nNumberArr.filter(isPrime);
  var resultNumEl = document.querySelector("#result-number");
  if (primeArr.length > 0) {
    return (resultNumEl.innerText = `Số nguyên tố đầu tiên: ${primeArr[0]}`);
  } else {
    return (resultNumEl.innerText = `Không có số nguyên tố, trả về: ${-1}`);
  }
}

function addRealNumToArr() {
  var realNumEl = document.querySelector("#real-number");
  var realNum = realNumEl.value * 1;
  realNumberArr.push(realNum);
  realNumEl.value = "";
  document.querySelector("#new-result-array").innerText = `${realNumberArr}`;
}

function countNumOfInterger() {
  var intArr = [];
  for (let i = 0; i < realNumberArr.length; i++) {
    if (Number.isInteger(realNumberArr[i])) {
      intArr.push(realNumberArr[i]);
    }
  }
  document.getElementById(
    "number-of-interger"
  ).innerText = `Số nguyên: ${intArr.length}`;
}

function compareArr() {
  var resultEl = document.getElementById("compared-result");
  var positiveNumArr = [];
  var negativeNumArr = [];
  for (let i = 0; i < nNumberArr.length; i++) {
    if (nNumberArr[i] > 0) {
      positiveNumArr.push(nNumberArr[i]);
    } else if (nNumberArr[i] < 0) {
      negativeNumArr.push(nNumberArr[i]);
    }
  }
  if (positiveNumArr.length > negativeNumArr.length) {
    resultEl.innerText = `Số dương > số âm`;
  } else if (positiveNumArr.length < negativeNumArr.length) {
    resultEl.innerText = `Số dương < số âm`;
  } else {
    resultEl.innerText = `Số dương = số âm`;
  }
}
